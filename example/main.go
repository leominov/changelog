package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/leominov/changelog"
)

var changelogFile = flag.String("changelog", "CHANGELOG.md", "Path to CHANGELOG.md file")

func main() {
	flag.Parse()
	body, err := ioutil.ReadFile(*changelogFile)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	log, err := changelog.Parse(body)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Printf("Releases: %d\n", len(log.Releases))
	for _, release := range log.Releases {
		fmt.Printf("Version %s changes: %d\n", release.Version, release.CalcChanges())
	}
}

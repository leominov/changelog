package changelog

import (
	"time"

	"github.com/coreos/go-semver/semver"
)

type Release struct {
	Version       string              `json:"version"`
	SemVersion    semver.Version      `json:"semversion"`
	DateRaw       string              `json:"date_original"`
	Date          time.Time           `json:"date"`
	Yanked        bool                `json:"yanked"`
	Changes       map[string][]string `json:"changes"`
	currentChange string
}

func NewRelease() *Release {
	return &Release{
		Changes: make(map[string][]string),
	}
}

func (r *Release) markAsUnreleased() {
	r.Version = unreleasedVersion
	r.DateRaw = time.Now().Format(dateLayout)
	r.Date = time.Now()
}

func (r *Release) CalcChanges() int {
	var sum int
	for _, changeEntries := range r.Changes {
		sum += len(changeEntries)
	}
	return sum
}

func (r *Release) Summary() map[string]int {
	summary := make(map[string]int, len(r.Changes))
	for changeName, changeEntries := range r.Changes {
		summary[changeName] = len(changeEntries)
	}
	return summary
}

func (r *Release) setBasic(version, date string) *Release {
	r.Version = version
	r.DateRaw = date
	v, err := semver.NewVersion(r.Version)
	if err == nil {
		r.SemVersion = *v
	}
	d, err := time.Parse(dateLayout, r.DateRaw)
	if err == nil {
		r.Date = d
	}
	return r
}

func (r *Release) IsPatchUpdateFor(release *Release) bool {
	if r.SemVersion.Major == release.SemVersion.Major &&
		r.SemVersion.Minor == release.SemVersion.Minor &&
		r.SemVersion.Patch > release.SemVersion.Patch {
		return true
	}
	return false
}

func (r *Release) IsMinorUpdateFor(release *Release) bool {
	if r.SemVersion.Major == release.SemVersion.Major &&
		r.SemVersion.Minor > release.SemVersion.Minor {
		return true
	}
	return false
}

func (r *Release) IsMajorUpdateFor(release *Release) bool {
	return r.SemVersion.Major > release.SemVersion.Major
}

package changelog

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"regexp"

	"github.com/coreos/go-semver/semver"
)

const (
	dateLayout        = "2006-01-02"
	unreleasedVersion = "Unreleased"
)

var (
	nontypedHeader   = regexp.MustCompile(`^#(.*)`)
	titleHeader      = regexp.MustCompile(`^# (.*)`)
	versionHeader    = regexp.MustCompile(`^## \[?([^]]+)\]? [-–] ([\d-–]{10})`)
	unreleasedHeader = regexp.MustCompile(`^## \[?[Uu]nreleased\]?`)
	yankedVersion    = regexp.MustCompile(`\[YANKED\]`)
	changesHeader    = regexp.MustCompile(`^### (.*)`)
	changeEntry      = regexp.MustCompile(`^[-*] (.*)`)
)

type Changelog struct {
	Title       string     `json:"title"`
	Description []string   `json:"description"`
	Releases    []*Release `json:"releases"`
}

func preFlightChecks(body []byte) error {
	if len(body) == 0 {
		return errors.New("Empty document")
	}
	return nil
}

func Parse(body []byte) (*Changelog, error) {
	if err := preFlightChecks(body); err != nil {
		return nil, err
	}
	c := &Changelog{}
	scanner := bufio.NewScanner(bytes.NewReader(body))
	for scanner.Scan() {
		line := scanner.Text()
		if len(line) == 0 {
			continue
		}
		c.processLine(line)
	}
	return c, nil
}

func (c *Changelog) addTitle(line string) {
	data := titleHeader.FindStringSubmatch(line)
	c.Title = data[1]
}

func (c *Changelog) addRelease(line string, unreleased bool) {
	release := NewRelease()
	if yankedVersion.MatchString(line) {
		release.Yanked = true
	}
	if !unreleased {
		data := versionHeader.FindStringSubmatch(line)
		release.setBasic(data[1], data[2])
	} else {
		release.markAsUnreleased()
	}
	if rel := c.GetRelease(release.Version); rel != nil {
		return
	}
	c.Releases = append(c.Releases, release)
}

func (c *Changelog) currentRelease() *Release {
	if len(c.Releases) == 0 {
		return nil
	}
	return c.Releases[len(c.Releases)-1]
}

func (c *Changelog) initChangeSet(line string) {
	r := c.currentRelease()
	if r == nil {
		return
	}
	data := changesHeader.FindStringSubmatch(line)
	name := data[1]
	r.currentChange = name
	r.Changes[name] = []string{}
}

func (c *Changelog) addChangeEntry(line string) {
	data := changeEntry.FindStringSubmatch(line)
	r := c.currentRelease()
	if r == nil {
		return
	}
	if _, ok := r.Changes[r.currentChange]; !ok {
		return
	}
	r.Changes[r.currentChange] = append(r.Changes[r.currentChange], data[1])
}

func (c *Changelog) isDescription(line string) bool {
	if nontypedHeader.MatchString(line) {
		return false
	}
	if changeEntry.MatchString(line) {
		return false
	}
	if len(c.Releases) > 0 {
		return false
	}
	return true
}

func (c *Changelog) processLine(line string) {
	if c.isDescription(line) {
		c.Description = append(c.Description, line)
	}
	if titleHeader.MatchString(line) {
		c.addTitle(line)
	}
	if versionHeader.MatchString(line) {
		c.addRelease(line, false)
	}
	if unreleasedHeader.MatchString(line) {
		c.addRelease(line, true)
	}
	if changesHeader.MatchString(line) {
		c.initChangeSet(line)
	}
	if changeEntry.MatchString(line) {
		c.addChangeEntry(line)
	}
}

func (c *Changelog) String() (string, error) {
	b, err := json.Marshal(c)
	return string(b), err
}

func (c *Changelog) Validate() error {
	if length := len(c.Releases); length == 0 {
		return errors.New("Empty releases list")
	} else if length == 1 && c.Releases[0].Version == unreleasedVersion {
		return errors.New("Unreleased version has no changes")
	}
	for _, release := range c.Releases {
		if release.CalcChanges() == 0 {
			return fmt.Errorf("Version %s has no changes", release.Version)
		}
	}
	return nil
}

func (c *Changelog) GetRelease(version string) *Release {
	for _, release := range c.Releases {
		if release.Version == version {
			return release
		}
	}
	return nil
}

func (c *Changelog) FindNewestThan(version string) ([]*Release, error) {
	ver, err := semver.NewVersion(version)
	if err != nil {
		return nil, err
	}
	newestReleases := []*Release{}
	for _, release := range c.Releases {
		if ver.LessThan(release.SemVersion) && !release.Yanked {
			newestReleases = append(newestReleases, release)
		}
	}
	return newestReleases, nil
}

func (c *Changelog) CalcReleaseChanges(version string) int {
	release := c.GetRelease(version)
	if release == nil {
		return 0
	}
	return release.CalcChanges()
}

func (c *Changelog) IsReleaseYanked(version string) bool {
	release := c.GetRelease(version)
	if release == nil {
		return false
	}
	return release.Yanked
}

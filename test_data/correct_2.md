# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2018-12-28
### Added
- Use default values when not defined in repository hash
- Added support for apt-key management

## [1.0.0] - 2018-05-31
### Added
- Apt append|remove on whole server's group added

### Changed
- Everything. This is first commit and release

### Removed
- Nothing
# Changelog

## [1.1.0] - 2019.03.05

При последующем обновлении, убедитесь, что exporter возвращает метрику `uptime`. Если метрики нет, то можно добавить её по аналогии с [этим PR](https://github.com/kbudde/rabbitmq_exporter/pull/116), если метрика имеет другое имя, то внесите изменение в дашборд [RabbitMQ Instance](http://grafana.devops.tcsbank.ru/d/000000078/rabbitmq-instance?refresh=5s&orgId=1).

### Изменено

* RabbitMQ Exporter обновлен до версии [0.29.0](https://github.com/kbudde/rabbitmq_exporter/tree/v0.29.0) + [GH-116](https://github.com/kbudde/rabbitmq_exporter/pull/116), в Pull Request добавлена метрика `uptime`, которая входит в часть базовых, но ранее exporter'ом не отдавалась.

## [1.0.1] - 2019.01.10

### Исправлено

* Указал конкретную версию в названии метароли.

## [1.0.0] - 2018.08.07

### Добавлено

* Публикация первой версии роли prometheus-rabbitmq-exporter.

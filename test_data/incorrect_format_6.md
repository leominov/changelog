Date: 2018-10-18

Version: `1.0.0`

Добавлена возможность указывать hostname запускаемого контейнера

Date: 2018-12-5

Version: `1.0.2`

Исправлена работа ansible под python3 - https://docs.ansible.com/ansible/2.7/user_guide/playbooks_python_version.html#dict-iteritems

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Unreleased
### Changed
- Consul agent installation
- Fix of "Specifying include variables at the top-level of the task is deprecated"
- Support 18.04 Ubuntu release

## [1.1.16] - 2019-02-13
### Changed
- Remove a.gitin

## [1.1.15] - 2019-02-05 [YANKED]
### Changed
- Remove NS-server rotation

## [1.1.14] - 2019-01-18
### Changed
- Fix loop multiple items on rhel7 instances

## [1.1.13] - 2018-12-27
### Removed
- Removed dda

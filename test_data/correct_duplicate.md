# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.2.3] - 2019-03-12

### Added

- required paramters with min and max wraping ttl

## [1.2.3] - 2019-03-11

### Changed

- Policy template update

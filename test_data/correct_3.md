# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2018-08-07
### Added
- Multiple ranchers support
 
  `Since now it is possible set as much ranchers and rancher creds combination as you want`

### Changed
- Task label. Will show only host labels dict
- Rancher credentials stored as map (dictionary)

### Removed
- Nothing

## [1.0.0] - 2018-06-20
### Added
- It is possible to set rancher host labels 

### Changed
- Everything. This is first commit and release

### Removed
- Nothing

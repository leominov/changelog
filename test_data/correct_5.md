# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

### Unreleased
- Consul agent installation
- Fix of `Specifying include variables at the top-level of the task is deprecated`
- Support 18.04 Ubuntu release

## [1.1.16] - 2019-02-13
### Changed
- Remove a.gitin

## [1.1.15] - 2019-02-05
### Changed
- Remove NS-server rotation

## [1.1.14] - 2019-01-18
### Changed
- Fix loop multiple items on rhel7 instances

## [1.1.13] - 2018-12-27
### Removed
- Removed dda

## [1.1.12] - 2018-12-27
### Added
- Added a.gitin

## [1.1.11] - 2018-12-07
### Changed
- Fix for timesyncd disabling step

### Removed
- User ktk

## [1.1.10] - 2018-11-27
### Changed
- Fix for /run/resolvconf/interfaces/ missing on Ubuntu instances

## [1.1.9] - 2018-11-22
### Changed
- Fix timesyncd disabling on rhel7 instances

## [1.1.8] - 2018-11-22
### Changed
- Fix for cert installation on RedHat6 instances

## [1.1.7] - 2018-11-02
### Added
- tcsbank.ru and tinkoff.cloud root CA certs
- default values for all environment variables except OS_PASSWORD
- INSTANCE_NAME and keypair name get date-suffix because of intersect issues

### Changed
- Nothing

### Removed
- Nothing

## [1.1.6] - 2018-11-02
### Added
- Nothing

### Changed
- DNS setup. Make several checks depending on the OS and Platform

### Removed
- Nothing

## [1.1.5] - 2018-11-01
### Added
- Nothing

### Changed
- Tests added: Assert managed by ansible
- Tests added: Check if python3-yaml is installed
- On tests puppet is disabled
- Installing python3-yaml

### Removed
- Nothing

## [1.1.4] - 2018-11-01
### Added
- Nothing

### Changed
- ntp.conf managed by puppet

### Removed
- Nothing


## [1.1.1-1.1.3]
### Added
- Adding users opa, dda, kmv, sky

### Changed
- Nothing

### Removed
- Nothing

## [1.0.1] - 2018-09-26
### Added
- List of engineers users
- Ability to install zabbix server and zabbix agent
- Zabbix tests removed
- Custom users and groups lists can be set at your environment
- Required ansible version is set to `2.5`
- Tests
- OpenStack configuration is moved to environment variables

### Changed
- Nothing

### Removed
- Nothing
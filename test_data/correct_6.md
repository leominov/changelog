# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
- Fixed changelog format

## [3.2.1] - 2019-03-12 RTFM policy templating
### Added
- required paramters with min and max wraping ttl
### Removed
- Nothing
### Changed
- Policy template update

## [3.2.0] - 2019-03-11 Policy templating
### Added
- Nothing
### Removed
- Nothing
### Changed
- Rewrote policy template allow use allowed_parameters and denied_parameters. It's bounded only to capabilities all other parameters will be converted to
 
      option = {
        "arg1" = ["first", "second"]
        "arg2" = []
      }
    

## [3.0.1] - 2018-12-13 Save secrets
### Added
- Nothing

### Removed
- Secrets engines unmount disabled

### Changed
- Fixed variables at README.md

## [3.0.0] - 2018-12-12 Big Brother
### Added
- Delete orphan policies
- Delete orphan secrets engines
- Delete orphan userpass users.Only /v1/auth/userpass will be checked for orphans
- Delete orphan approles. Only for /v1/auth/approle
- Delete orphan ldap groups. Only for /v1/auth/ldap
- Delete orphan auth backends
- Delete orphan audit devices
- Makefile tasks for managing local dev vault instance
- Add ability enabling and disabling audit devices

### Removed
- Nothing

### Changed
- Secrets backends renamed to Secrets Engines
- Path to roles appended for testing while developing
- Default policies updated for managing audit devices

## [2.0.1] - 2018-11-29 200 when kv upgrade
### Deprication
- Secrets backends will be renamed to secrets engines at 3.0.0

### Added
- Nothing

### Changed
- Added 200 status code on tune secrets engine

### Removed
- Nothing

## [2.0.0] - 2018-11-23 Role authorization
### Added
- Role userpass authorization
- Added yamllint
- Added new variable vault_config_applicator_creds
```
  vault_config_applicator_creds:
    user: <username>
    pass: <password>
```
- ansible-lint and playbook test apply

### Changed
- Global config variables: policies, backends, storages, approles, userpass, ldap groups should be defined as list
``` 
  policies:
    - "{{ project_1_policies }}"
    - "{{ project_2_policies }}"
```

- Fixed bug with failing on deleting absent userpass user's dynamic policy.

### Removed
- vault_config_applicator_token variable

## [1.2.0] - 2018-09-18 AD integration
### Added
- Ability to configure ldap auth backend
- Ability to bound LDAP groups with policies

### Changed
- Added ldap example to [README](README.md)
- Fixed userpass user password generation

### Removed
- Nothing

## [1.1.1] - 2018-07-06
### Added
- Nothing

### Changed
- Fixed userpass users parameter at README.md
- Fixed adding to user dynamic userpass password policy

### Removed
- Nothing

## [1.1.0] - 2018-07-05
### Added
- Nothing

### Changed
- Configs unified
- Name added to task header (cosmetic change)
- Auth backend update fixed (did not work at all)
- Name from params used as part of path. Root level name parameter can be used as comment
- Userpass password dynamic policy for every userpass user will be generated

### Removed
- Nothing

## [1.0.0] - 2018-06-27
### Added
- You can mount|unmount with this role auth backends
- You can mount|unmount with this role secrets backends. Tested on KV
- You can create|update|delete policies
- You can create|update|delete userpass users
- You can create|update|delete approles
- User will be created with specified password or with default if not set

### Changed
- Everything. This is very first release

### Removed
- Nothing
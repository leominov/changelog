package changelog

import (
	"testing"

	"github.com/coreos/go-semver/semver"
)

func TestSummary(t *testing.T) {
	r := &Release{}
	sum := r.Summary()
	if len(sum) != 0 {
		t.Errorf("Must be 0, but got %d", len(sum))
	}
	r = &Release{
		Changes: map[string][]string{
			"Deleted": []string{
				"Change 1",
				"Change 2",
			},
			"Added": []string{
				"Change 3",
			},
		},
	}
	sum = r.Summary()
	if len(sum) != 2 {
		t.Errorf("Must be 2, but got %d", len(sum))
	}
}

func TestIsPatchUpdateFor(t *testing.T) {
	tests1 := []struct {
		ver1, ver2 string
		update     bool
	}{
		{
			ver1:   "1.0.0",
			ver2:   "1.0.0",
			update: false,
		},
		{
			ver1:   "1.0.0",
			ver2:   "1.0.1",
			update: true,
		},
		{
			ver1:   "1.0.0",
			ver2:   "1.2.0",
			update: false,
		},
		{
			ver1:   "1.0.0",
			ver2:   "2.3.0",
			update: false,
		},
	}
	for id, test := range tests1 {
		r1 := &Release{
			SemVersion: *semver.New(test.ver1),
		}
		r2 := &Release{
			SemVersion: *semver.New(test.ver2),
		}
		if r2.IsPatchUpdateFor(r1) != test.update {
			t.Errorf("%d. Must be %v, but got %v", id, test.update, !test.update)
		}
	}
}

func TestIsMinorUpdateFor(t *testing.T) {
	tests1 := []struct {
		ver1, ver2 string
		update     bool
	}{
		{
			ver1:   "1.0.0",
			ver2:   "1.0.0",
			update: false,
		},
		{
			ver1:   "1.0.0",
			ver2:   "1.0.1",
			update: false,
		},
		{
			ver1:   "1.0.0",
			ver2:   "1.2.0",
			update: true,
		},
		{
			ver1:   "1.0.0",
			ver2:   "2.3.0",
			update: false,
		},
	}
	for id, test := range tests1 {
		r1 := &Release{
			SemVersion: *semver.New(test.ver1),
		}
		r2 := &Release{
			SemVersion: *semver.New(test.ver2),
		}
		if r2.IsMinorUpdateFor(r1) != test.update {
			t.Errorf("%d. Must be %v, but got %v", id, test.update, !test.update)
		}
	}
}

func TestIsMajorUpdateFor(t *testing.T) {
	tests1 := []struct {
		ver1, ver2 string
		update     bool
	}{
		{
			ver1:   "1.0.0",
			ver2:   "1.0.0",
			update: false,
		},
		{
			ver1:   "1.0.0",
			ver2:   "1.0.1",
			update: false,
		},
		{
			ver1:   "1.0.0",
			ver2:   "1.2.0",
			update: false,
		},
		{
			ver1:   "1.0.0",
			ver2:   "2.3.0",
			update: true,
		},
	}
	for id, test := range tests1 {
		r1 := &Release{
			SemVersion: *semver.New(test.ver1),
		}
		r2 := &Release{
			SemVersion: *semver.New(test.ver2),
		}
		if r2.IsMajorUpdateFor(r1) != test.update {
			t.Errorf("%d. Must be %v, but got %v", id, test.update, !test.update)
		}
	}
}

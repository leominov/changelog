# Changelog Parser

[![BuildStatus Widget]][BuildStatus Result]
[![codecov](https://codecov.io/gh/leominov/changelog/branch/master/graph/badge.svg)](https://codecov.io/gh/leominov/changelog)

[BuildStatus Result]: https://travis-ci.com/leominov/changelog
[BuildStatus Widget]: https://travis-ci.com/leominov/changelog.svg?branch=master

Specification based on [Keep a Changelog 1.0.0](https://keepachangelog.com/en/1.0.0/) format.

## Example

See [example](example) folder.

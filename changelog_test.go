package changelog

import (
	"io/ioutil"
	"testing"
)

func TestParse(t *testing.T) {
	if _, err := Parse([]byte("")); err == nil {
		t.Error("Must be an error, but got nil")
	}
	tests := []struct {
		path, title       string
		description       []string
		releases, changes int
	}{
		{
			path:  "test_data/correct_1.md",
			title: "Changelog",
			description: []string{
				"All notable changes to this project will be documented in this file.",
				"The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)",
				"and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).",
			},
			releases: 5,
		},
		{
			path:  "test_data/correct_2.md",
			title: "Changelog",
			description: []string{
				"All notable changes to this project will be documented in this file.",
				"The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)",
				"and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).",
			},
			releases: 2,
		},
		{
			path:  "test_data/correct_3.md",
			title: "Changelog",
			description: []string{
				"All notable changes to this project will be documented in this file.",
				"The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)",
				"and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).",
			},
			releases: 2,
		},
		{
			path:  "test_data/correct_4.md",
			title: "Changelog",
			description: []string{
				"Журнал изменений роли Docker.",
			},
			releases: 5,
		},
		{
			path:  "test_data/correct_5.md",
			title: "Changelog",
			description: []string{
				"All notable changes to this project will be documented in this file.",
				"The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)",
				"and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).",
			},
			releases: 14,
		},
		{
			path:  "test_data/correct_6.md",
			title: "Changelog",
			description: []string{
				"All notable changes to this project will be documented in this file.",
				"The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)",
				"and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).",
			},
			releases: 11,
		},
	}
	for _, test := range tests {
		b, err := ioutil.ReadFile(test.path)
		if err != nil {
			t.Error(err)
		}
		chlog, err := Parse(b)
		if err != nil {
			t.Error(err)
		}
		if chlog.Title != test.title {
			t.Errorf("Must be %s, but got %s", test.title, chlog.Title)
		}
		if len(chlog.Description) != len(test.description) {
			t.Errorf("Must be %d, but got %d", len(test.description), len(chlog.Description))
		}
		for id, line := range chlog.Description {
			if line != test.description[id] {
				t.Errorf("Must be %s, but got %s", test.description[id], line)
			}
		}
		if len(chlog.Releases) != test.releases {
			t.Errorf("Must be %d, but got %d", test.releases, len(chlog.Releases))
		}
	}
}

func TestParse_Duplicate(t *testing.T) {
	b, err := ioutil.ReadFile("test_data/correct_duplicate.md")
	if err != nil {
		t.Error(err)
	}
	chlog, err := Parse(b)
	if err != nil {
		t.Error(err)
	}
	if len(chlog.Releases) != 1 {
		t.Errorf("Must be 1, but got %d", len(chlog.Releases))
	}
}

func TestParse_Incorrect(t *testing.T) {
	tests := []string{
		"test_data/incorrect_format_1.md",
		"test_data/incorrect_format_2.md",
		"test_data/incorrect_format_3.md",
		"test_data/incorrect_format_4.md",
		"test_data/incorrect_format_5.md",
		"test_data/incorrect_format_6.md",
		"test_data/incorrect_format_7.md",
	}
	for _, test := range tests {
		b, err := ioutil.ReadFile(test)
		if err != nil {
			t.Error(err)
		}
		chlog, err := Parse(b)
		if err != nil {
			t.Error(err)
		}
		release := chlog.GetRelease("1.0.0")
		if release != nil {
			t.Errorf("Must be nil, but got %v", release)
		}
	}
}

func TestGetRelease(t *testing.T) {
	tests := []struct {
		path                      string
		nonNilVersion, nilVersion string
	}{
		{
			path:          "test_data/correct_1.md",
			nonNilVersion: "Unreleased",
			nilVersion:    "2.0.0",
		},
		{
			path:          "test_data/correct_2.md",
			nonNilVersion: "1.0.0",
			nilVersion:    "Unreleased",
		},
	}
	for _, test := range tests {
		b, err := ioutil.ReadFile(test.path)
		if err != nil {
			t.Error(err)
		}
		chlog, err := Parse(b)
		if err != nil {
			t.Error(err)
		}
		if chlog.GetRelease(test.nonNilVersion) == nil {
			t.Error("Must be non-nil, but git nil")
		}
		if chlog.GetRelease(test.nilVersion) != nil {
			t.Error("Must be nil, but git non-nil")
		}
	}
}

func TestFindNewestThan(t *testing.T) {
	tests := []struct {
		path, version string
		newest        int
	}{
		{
			path:    "test_data/correct_1.md",
			version: "1.1.15",
			newest:  1,
		},
		{
			path:    "test_data/correct_2.md",
			version: "1.1.0",
			newest:  0,
		},
	}
	for _, test := range tests {
		b, err := ioutil.ReadFile(test.path)
		if err != nil {
			t.Error(err)
		}
		chlog, err := Parse(b)
		if err != nil {
			t.Error(err)
		}
		if _, err := chlog.FindNewestThan("FOOBAR"); err == nil {
			t.Error("Must be error, but got nil")
		}
		releases, err := chlog.FindNewestThan(test.version)
		if err != nil {
			t.Error(err)
		}
		if len(releases) != test.newest {
			t.Errorf("Must be %d, but got %d", test.newest, len(releases))
		}
	}
}

func TestCalcReleaseChanges(t *testing.T) {
	tests := []struct {
		path, version string
		changes       int
	}{
		{
			path:    "test_data/correct_1.md",
			version: "1.1.16",
			changes: 1,
		},
		{
			path:    "test_data/correct_2.md",
			version: "1.0.0",
			changes: 3,
		},
		{
			path:    "test_data/correct_3.md",
			version: "1.1.0",
			changes: 4,
		},
		{
			path:    "test_data/correct_4.md",
			version: "1.1.0",
			changes: 6,
		},
		{
			path:    "test_data/correct_5.md",
			version: "2.0.0",
			changes: 0,
		},
		{
			path:    "test_data/correct_6.md",
			version: "Unreleased",
			changes: 0,
		},
		{
			path:    "test_data/correct_7.md",
			version: "Unreleased",
			changes: 1,
		},
	}
	for _, test := range tests {
		b, err := ioutil.ReadFile(test.path)
		if err != nil {
			t.Error(err)
		}
		chlog, err := Parse(b)
		if err != nil {
			t.Error(err)
		}
		if chlog.CalcReleaseChanges(test.version) != test.changes {
			t.Errorf("Must be %d, but got %d", test.changes, chlog.CalcReleaseChanges(test.version))
		}
	}
}

func TestIsReleaseYanked(t *testing.T) {
	tests := []struct {
		path, version string
		yanked        bool
	}{
		{
			path:    "test_data/correct_1.md",
			version: "1.1.15",
			yanked:  true,
		},
		{
			path:    "test_data/correct_2.md",
			version: "1.1.0",
			yanked:  false,
		},
		{
			path:    "test_data/correct_3.md",
			version: "2.0.0",
			yanked:  false,
		},
	}
	for _, test := range tests {
		b, err := ioutil.ReadFile(test.path)
		if err != nil {
			t.Error(err)
		}
		chlog, err := Parse(b)
		if err != nil {
			t.Error(err)
		}
		if chlog.IsReleaseYanked(test.version) != test.yanked {
			t.Errorf("Must be %v, but got %v", test.yanked, !test.yanked)
		}
	}
}

func TestValidate(t *testing.T) {
	c := &Changelog{}
	if err := c.Validate(); err == nil {
		t.Error("Must be an error, but got nil")
	}
	c.Releases = []*Release{
		&Release{
			Version: "Unreleased",
			Changes: make(map[string][]string),
		},
	}
	if err := c.Validate(); err == nil {
		t.Error("Must be an error, but got nil")
	}
	c.Releases = []*Release{
		&Release{
			Version: "Unreleased",
			Changes: make(map[string][]string),
		},
		&Release{
			Version: "1.0.0",
			Changes: make(map[string][]string),
		},
	}
	if err := c.Validate(); err == nil {
		t.Error("Must be an error, but got nil")
	}
	c.Releases = []*Release{
		&Release{
			Version: "Unreleased",
			Changes: map[string][]string{
				"Changed": []string{
					"Foobar",
				},
			},
		},
		&Release{
			Version: "1.0.0",
			Changes: map[string][]string{
				"Added": []string{
					"Foobar",
				},
			},
		},
	}
	if err := c.Validate(); err != nil {
		t.Errorf("Must be nil, but got %v", err)
	}
}

func TestString(t *testing.T) {
	c := &Changelog{}
	s, err := c.String()
	if err != nil {
		t.Error(err)
	}
	if s == "" {
		t.Errorf("Must be non-empty string, but got it")
	}
}
